package com.example.mp;

import com.example.mp.domain.User;
import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-18 22:01
 */
@SpringBootTest
public class TestSelectBatchIds {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void test() {
        //根据id集合批量查询
        List<User> userList = userMapper.selectBatchIds(List.of(1, 2, 3));
        System.out.println("userList = " + userList);
    }

}

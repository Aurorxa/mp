package com.example.mp;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mp.domain.User;
import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-19 11:20
 */
@SpringBootTest
public class TestEq {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void test() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("password", "123456")
                .ge("age", 20)
                .in("name", "张三", "李四", "王五");

        List<User> userList = userMapper.selectList(wrapper);
        System.out.println("userList = " + userList);
    }

}

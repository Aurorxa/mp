package com.example.mp;

import com.example.mp.domain.User;
import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-18 21:00
 */
@SpringBootTest
public class TestSelectById {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void test(){
        //根据id查询数据
        User user = userMapper.selectById(6);
        System.out.println("user = " + user);
    }
}

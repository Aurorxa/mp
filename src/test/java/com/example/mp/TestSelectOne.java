package com.example.mp;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.mp.domain.User;
import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-18 22:08
 */
@SpringBootTest
public class TestSelectOne {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void test() {
        LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(User::getUsername, "zhangsan");

        //根据条件查询一条数据，如果结果超过一条则报错
        User user = userMapper.selectOne(lambdaQueryWrapper);
        System.out.println("user = " + user);
    }


}

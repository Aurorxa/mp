package com.example.mp;

import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-18 20:39
 */
@SpringBootTest
public class TestDeleteBatchIds {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void test() {
        //根据id集合批量删除
        int result = userMapper.deleteBatchIds(List.of(1, 2, 3));
        System.out.println("result = " + result);
    }
}

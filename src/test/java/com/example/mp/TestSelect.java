package com.example.mp;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mp.domain.User;
import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-19 13:48
 */
@SpringBootTest
public class TestSelect {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void test() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();

        //SELECT user_name,age,password FROM tb_user WHERE (name = ? OR age = ?)
        wrapper
                .eq("name", "张三")
                .or()
                .eq("age", 18)
                .select("user_name", "age", "password");

        List<User> userList = this.userMapper.selectList(wrapper);
        System.out.println("userList = " + userList);
    }
}

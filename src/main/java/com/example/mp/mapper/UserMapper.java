package com.example.mp.mapper;

import com.example.mp.config.MyBaseMapper;
import com.example.mp.domain.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-17 21:10
 */
@Mapper
public interface UserMapper extends MyBaseMapper<User> {

}

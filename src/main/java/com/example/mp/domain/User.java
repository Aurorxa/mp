package com.example.mp.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.example.mp.enums.SexEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-17 21:01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
public class User implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    @TableField("user_name") //解决字段名不一致
    private String username;

    private String password;

    private String name;

    private Integer age;

    private String email;

    //性别为枚举
    private SexEnum sex;

    @Version
    @TableField(value = "version", fill = FieldFill.INSERT)
    private Integer version;

    /**
     * 为createTime添加自动填充功能，在新增数据的时候有效
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 为updateTime添加自动填充功能，在更新数据的时候有效
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;

    @TableLogic
    private Integer deleted;


}
